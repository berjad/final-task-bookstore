export const environment = {
  production: false,
  apiUrl: 'https://scania-bookstore.herokuapp.com'
};
