import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SessionService } from 'src/app/services/session/session.service';
import { Router } from '@angular/router';

function passwordsMatchValidator(form) {
  const password = form.get('password')
  const confirmPassword = form.get('cpassword')

  if(password.value !== confirmPassword.value) {
    confirmPassword.setErrors({ passwordsMatch: true })
  } else {
    confirmPassword.setErrors(null)
  }

  return null
}



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup 
  isLoading: boolean = false;

  registerError: string;

  constructor(
      private authService: AuthService, 
      private session: SessionService, 
      private router: Router,
      private builder: FormBuilder) {
    if(this.session.get() !== false) {
      this.router.navigateByUrl('/catalog');
    }

   }


  ngOnInit() {
    this.buildForm()
    console.log(this.cpassword);
    
  }

  buildForm() {
      this.registerForm = this.builder.group({
      email: ['', [Validators.required, Validators.email]],
      full_name: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      cpassword: ''
      }, {
        validators: passwordsMatchValidator
      
    })
  
  }

  get email () {
    return this.registerForm.get('email');
  }

  get full_name () {
    return this.registerForm.get('full_name');
  }

  get password () {
    return this.registerForm.get('password');
  }

  get cpassword () {
    return this.registerForm.get('cpassword');
  }
  
  async onRegisterClicked() {

    this.registerError = '';

    try {
      this.isLoading = true;
      const result: any = await this.authService.register(this.registerForm.value);

      if (result.status < 400) {
        this.session.save({
          token: result.data.token,
          full_name: result.data.user.full_name,
          email: result.data.user.email
        });
        this.router.navigateByUrl('/catalog')
      }

    } catch(e) {
      console.error(e.error);
      this.registerError = e.error.error
      
    } finally {
      this.isLoading = false;
    }
  }
}

