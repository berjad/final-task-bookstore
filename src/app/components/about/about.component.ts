import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(private session: SessionService) { }

  ngOnInit(): void {
  }

  displayBreadcrumb() {
    if (this.session.get() !== false) {
      return true;
    } else {
      return false;
    }
  }

}
