import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session/session.service';
import { BooksService } from 'src/app/services/books/books.service';
import { Book } from 'src/app/models/book';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {

  inStock = true

  postForm: FormGroup = new FormGroup({ 
    title: new FormControl('', [Validators.required]),
    authors: new FormControl('', [Validators.required]),
    cover: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    stock: new FormControl('', [Validators.required]),
    isbn: new FormControl('', [Validators.required])
  });

  _listFilter: string;
  get listFilter(): string {
      return this._listFilter;
  }
  set listFilter(value:string) {
    this._listFilter = value;
    this.filteredBooks = this.listFilter ? this.performFilter(this.listFilter) : this.books;
  }

  performFilter(filterBy: string): Book[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.books.filter((products: Book) =>
  products.authors.toLocaleLowerCase().indexOf(filterBy) !== -1 || products.title.toLocaleLowerCase().indexOf(filterBy) !== -1);
  //  || (products.title.toLocaleLowerCase().indexOf(filterBy) !== -1));
}


  books: Book [] = [];
  filteredBooks: Book[];


  constructor(private session: SessionService, private booksService: BooksService, private router: Router) { }

  async ngOnInit() {
    console.log('inStockValue: ' + this.inStockOnly);
    console.log('checkBoxValue: ' + this.checkBoxValue);
    try {
      const result: any = await this.booksService.getBooks()
        this.books=result.data
        this.filteredBooks = this.books;
        
      
    } catch(e) {
      console.error(e);
      
    }


  }

  get full_name() {
    return this.session.get().full_name;
  }

  get email() {
    return this.session.get().email;
  }

  get last_login() {
    return this.session.get().last_login;
  }


  onAddBookClicked() {
    this.router.navigate(['/add-book'])
  }

  onEditClicked() {
    this.router.navigate(['/edit-user'])
  }

  async onDeleteClicked(id) {

  const deleteBookConfirm = confirm('Are you sure you want to delete this book?')
    if( deleteBookConfirm == true) {
    try {
      const result = await this.booksService.deleteBook(id);
      return true;
    } catch(e) {
      console.error(e);
      
    } finally {
      location.reload();
    }
  } else {
    return false;
  }
}
  
  onLogoutClicked() {
    const logoutConfirm = confirm('Are you sure you want to logout?')

    if( logoutConfirm == true) {
      localStorage.removeItem('book_session');
      this.router.navigate(['/login']);
      return true
    } else {
      return false;
    }
  }

  inStockOnly:boolean = false  
  checkBoxValue:boolean = false

  changeCheckBox(e:any, inStockOnly: boolean) {
    if(e.target.checked) {
      this.checkBoxValue = true
      inStockOnly = this.checkBoxValue

      console.log(inStockOnly + ' inStockOnly Checked');
      console.log(this.checkBoxValue + ' checkBoxValue Checked');
      
    } else {
      this.checkBoxValue= false
      inStockOnly = this.checkBoxValue
      console.log(inStockOnly + ' inStockOnly Checked');
      console.log(this.checkBoxValue + ' checkBoxValue Checked');

    }
  }
}
