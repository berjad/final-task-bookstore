import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogComponent } from './catalog.component';
import { AddBookComponent } from './add-book/add-book.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common'

const routes: Routes = [
    {
        path: '',
        component: CatalogComponent
    }
];

@NgModule({
    imports: [ RouterModule.forChild( routes ), ReactiveFormsModule, FormsModule, CommonModule ],
    exports: [ RouterModule ],
    declarations: [AddBookComponent, EditUserComponent]
})

export class CatalogModule {}