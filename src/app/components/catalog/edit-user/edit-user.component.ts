import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users/users.service';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  editForm: FormGroup = new FormGroup({
    full_name: new FormControl('', [Validators.required]),
    email: new FormControl( this.email ),
  });

  isLoading: boolean = false;

  updateUserError: string;

  updateUserSuccess: boolean = false;

  constructor(private router: Router, private usersService: UsersService, private session: SessionService) { }

  ngOnInit(): void {
  }

  get email() {
    return this.session.get().email;
  }

  onCancelClicked() {
    this.router.navigate(['/catalog'])
  }

  async updateName() {

    this.updateUserError = '';

    try {
      this.isLoading = true;
      const result: any = await this.usersService.updateFullname(this.editForm.value)

      if(result.status < 205) {
        this.updateUserSuccess = true;

        const session = this.session.get();
        this.session.save({
        ...session,
        full_name: result.data.full_name
});
      }
    } catch (e) {
      this.updateUserError = e;
    } finally {
      this.isLoading = false;

    }
  }
}
