import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AddBookComponent } from './add-book.component';

const routes: Routes = [
    {
        path: '',
        component: AddBookComponent
    }
];

@NgModule({
    imports: [ RouterModule.forChild( routes ), CommonModule ],
    exports: [ RouterModule ]
})

export class AddBookModule {}
