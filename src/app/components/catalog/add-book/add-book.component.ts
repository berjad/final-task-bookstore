import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import { BooksService } from 'src/app/services/books/books.service';
import { Router } from '@angular/router';

function lowestBookQuantity(form) {
  const stockQuantity = form.get('stock')
  if(stockQuantity.value<0) {
    stockQuantity.setErrors({ inCorrect: true })
  } else {
    stockQuantity.setErrors(null)
  }

  return null
}

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  postForm: FormGroup;


  constructor(private booksService: BooksService, private router: Router, private builder: FormBuilder) { }
  isLoading: boolean = false;

  addBookError: string;

  addBookSuccess: boolean = false;

  ngOnInit(): void {
    this.buildForm()
  }

  buildForm() {
    this.postForm = this.builder.group({
      title: ['', Validators.required],
      authors: ['', Validators.required],
      cover: ['', Validators.required],
      description: ['', Validators.required],
      stock: ['', Validators.required],
      isbn: ['', Validators.required]
    }, {
      validators: lowestBookQuantity
    })
  }

  onCancelClicked() {
    this.router.navigate(['/catalog'])
  }

  async addNewBook() {

    this.addBookError = '';

    try {
      this.isLoading = true;
      const result: any = await this.booksService.addBook(this.postForm.value)
      console.log(result);

      if(result.status < 205) {
        this.addBookSuccess = true;
      }
    } catch (e) {
      console.error(e.error)
      this.addBookError = e.error.error
    } finally {
      this.isLoading = false;
     
    }
  } 

}
