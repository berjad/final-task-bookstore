import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SessionService } from 'src/app/services/session/session.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({ 
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)])
  });

  isLoading: boolean = false;
  loginError: string;

  constructor(private session: SessionService, private router: Router, private authService: AuthService) {
    if(this.session.get() !== false) {
      this.router.navigateByUrl('/catalog');
    }
   }

  ngOnInit(): void {
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

  async onLoginClicked() {
    this.loginError = '';

    try {
      this.isLoading = true;
      const result: any = await this.authService.login(this.loginForm.value); 
      console.log(result);
      

      if(result.status < 400) {
        this.session.save({
          token: result.data.token,
          email: result.data.user.email,
          full_name: result.data.user.full_name,
          last_login: result.data.user.last_login
        });
        this.router.navigateByUrl('/catalog');
      }

    } catch(e) {
      this.loginError = e;
    } finally {
      this.isLoading = false;
    }
  }
}
