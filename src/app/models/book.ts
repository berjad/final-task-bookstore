

export class Book {
    id?: number;
    title: string;
    authors: string;
    description: string;
    cover: string;
    stock: number;
    isbn: number;
}
