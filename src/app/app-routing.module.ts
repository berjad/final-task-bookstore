import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './guards/auth/auth.guard';
import { AddBookComponent } from './components/catalog/add-book/add-book.component';
import { EditUserComponent } from './components/catalog/edit-user/edit-user.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'add-book',
    loadChildren: ()=> import('./components/catalog/add-book/add-book.module').then(m => m.AddBookModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: 'edit-user',
    loadChildren: ()=> import('./components/catalog/edit-user/edit-user.module').then(m => m.EditUserModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: 'about',
    loadChildren: ()=> import('./components/about/about.module').then(m => m.AboutModule),
  },
  {
    path: 'catalog',
    loadChildren: ()=> import('./components/catalog/catalog.module').then(m => m.CatalogModule),
    canActivate: [ AuthGuard ]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
