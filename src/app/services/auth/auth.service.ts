import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  register(user): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/register`, {
      // full_name: user.full_name,
      user: {...user}
    }).toPromise();
  }

  login(user): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/users/login`, {
      user: {...user}
      
    }).toPromise();
  }
}
