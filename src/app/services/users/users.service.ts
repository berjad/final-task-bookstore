import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SessionService } from '../session/session.service';
import { User } from 'src/app/models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient, private session: SessionService) { }

  updateFullname(user: User): Promise<any> {
    return this.http.patch(`${environment.apiUrl}/v1/api/users`, {
      "user": {
        full_name: user.full_name
      },

    },
    {
      headers: {
        'Authorization': 'Bearer ' + this.session.get().token,
        'Content-Type': 'application/json'
      }
    }).toPromise();
  }
}
