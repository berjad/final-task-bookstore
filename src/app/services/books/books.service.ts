import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SessionService } from '../session/session.service';
import { Book } from 'src/app/models/book';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(private http: HttpClient, private session: SessionService) { }


  getBooks(): Promise<any> {
  return this.http.get(`${environment.apiUrl}/v1/api/books`, {
    headers: {
      'Authorization': 'Bearer ' + this.session.get().token
    }
  }).toPromise();
  }


  addBook(book: Book): Promise<any> {
    return this.http.post(`${environment.apiUrl}/v1/api/books`, {
      "book": {
        title: book.title,
        authors: book.authors,
        cover: book.cover,
        description: book.description,
        stock: book.stock,
        isbn: book.isbn
      },

    },
    
    {
      headers: {
        'Authorization': 'Bearer ' + this.session.get().token,
        'Content-Type': 'application/json'
      }
    }).toPromise();
  }

  deleteBook(id): Promise<any> {
    return this.http.delete(`${environment.apiUrl}/v1/api/books/${id}`, {
      headers: {
        'Authorization': 'Bearer ' + this.session.get().token,
        'Content-Type': 'application/json'
      }
    }).toPromise();
  }
}